package main.java.ds;

import java.util.Iterator;

public class DynArrayIt<T> implements Iterator<T> {
    private DynArray<T> arr;
    private int posNextElement = 0;
    
    DynArrayIt(DynArray<T> pArr){
        this.arr = pArr;
    }
    
    @Override
    public boolean hasNext() {
        return (posNextElement < this.arr.size());
    }

    @Override
    public T next() {
        return (T) this.arr.get(posNextElement++);
    }
    
}
