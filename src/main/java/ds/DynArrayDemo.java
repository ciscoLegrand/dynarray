
package main.java.ds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class DynArrayDemo {

    public static void main(String[] args) {        
        test12();
        test13();
    }
    
    public static void test12(){
        DynArray<String> d1 = new DynArray<>(); 
        System.out.println("d1[" + d1.size() + "]: " + d1); 

        d1.add("hi"); 
        d1.add("bye"); 
        System.out.println(d1.getLength());
        System.out.println("d1[" + d1.size() + "]: " + d1); 

        DynArray<Integer> d2 = new DynArray<>(new Integer[]{11, 12, 14}); 
        System.out.println("d2[" + d2.size() + "]: " + d2); 

        DynArray<Integer> d3 = new DynArray<>(d2); 
        System.out.println("d3[" + d3.size() + "]: " + d3); 

        d2.add(2, 13); 
        d3.clear(); 
        System.out.println("d2[" + d2.size() + "]: " + d2); 
        System.out.println("d3[" + d3.size() + "]: " + d3); 

        d1.set(0, "phone");  
        d1.delete("bye"); 
        d1.add("home!"); 
        System.out.println("d1[" + d1.size() + "]: " + d1); 

        for(int i=0; i<d2.size(); i++) 
            System.out.println(d2.get(i)); 

    }
    public static void test13(){
        DynArray<Integer> d4 = new DynArray<>(new String[]{"hi", "by"}); 
 
        System.out.println("d4[" + d4.size() + "]: " + d4); 

        for(int i=0; i<d4.size(); i++) 
            System.out.println(d4.get(i)); 

        for(Object n : d4) 
            System.out.println(n);
        
//        for(int n : d4)
//            System.out.println(n);
    }
}
