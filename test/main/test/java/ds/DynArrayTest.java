/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.test.java.ds;

import main.java.ds.DynArray;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cisco
 */
public class DynArrayTest {
    
    public DynArrayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @Test
    public void testForEach(){
        System.out.println("16-> comprobar funcionamiento for-each");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);
        assertEquals(3,instance.size());
        int cont=0;
        for (Object el : instance){
            cont++;
        }
        assertEquals(3,cont);
    }
    
    
    @Test
    public void testObjSinParametro() {
        System.out.println("1-> Tamaño del objeto creado por defecto");
        DynArray instance = new DynArray();
        int expResult = 10;
        int result = instance.getLength();
        assertEquals(expResult, result);
    }

    @Test (expected = java.lang.NegativeArraySizeException.class)
    public void testObjParTamaño() throws IllegalArgumentException{
        System.out.println("2-> Tamaño del objeto creado con tamaño por parametro 1,4,7,15, -1");
        
        DynArray instance1 = new DynArray(1);
        DynArray instance2 = new DynArray(4);
        DynArray instance3 = new DynArray(7);
        DynArray instance4 = new DynArray(15);        
        int expResult1 = 1;
        int result1 = instance1.getLength();
        int expResult2 = 4;
        int result2 = instance2.getLength();
        int expResult3 = 7;
        int result3 = instance3.getLength();
        int expResult4 = 15;
        int result4 = instance4.getLength();
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        
        DynArray instance = new DynArray(-1);
        String expResult = "Tamaño no válido";
        NegativeArraySizeException result5 = new NegativeArraySizeException("Tamaño no válido");
        assertEquals(expResult, result5);
    }
   
    
    @Test
    public void testObjParamArray() {
        System.out.println("3-> Tamaño del objeto creado a partir de otro array");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);
        int expResult = 3;
        int result = instance.getLength();
        assertEquals(expResult, result);
    }
    @Test
    public void testObjParamObj() {
        System.out.println("4-> La cadena dela instancia debe ser ininciada como el obj1");
        Object[] s = {"uno","dos","cuatro"};
        DynArray obj = new DynArray(s);
        DynArray instance = new DynArray(obj);
//        assertTrue(instance.equals(obj));
        assertEquals(obj.toString(),instance.toString());
    }
    
     //devuelve el número de elementos contenidos en el array dinámico 
    @Test
    public void testSize (){
        System.out.println("5-> Obtiene cuantos elementos contiene el array");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);
        int expResult = 3;
        int result = instance.size();
        assertEquals(expResult, result);
        instance.add("tres");
        int expResult2 = 4;
        int result2 = instance.size();
        assertEquals(expResult2, result2);
        instance.add(0,"siete");
        int expResult3 = 5;
        int result3 = instance.size();
        assertEquals(expResult3, result3);
    }
    
     // indica si el array está vacío o no 
    @Test
    public void testIsEmpty (){
        System.out.println("6-> Indica si el array esta vacío");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);
        boolean result = instance.isEmpty();
        assertFalse(result);
        
        DynArray instance2 = new DynArray();
        boolean result2 = instance2.isEmpty();
        assertTrue(result2);
    }
    
      // vacía el array 
      @Test
    public void testClear (){
        System.out.println("7-> Vacía el array");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);              
        int expResult1 = 3;
        int result1 = instance.size();
        assertEquals(expResult1, result1);        
        
        instance.clear();     
        int expResult2=0;
        int result2=instance.size();
        assertEquals(expResult2,result2);
        assertEquals(3,instance.getLength());
        boolean result = instance.isEmpty();       
        assertTrue("el objeto aun tiene elementos",result);                
    }
//    
//        //devuelve la cadena localizada en la posición indicada del 
//    //array. En caso de que dicha posición esté fuera de rango, se lanzará 
//    //una excepción ​IndexOutOfBoundsException 
    @Test  (expected = java.lang.IndexOutOfBoundsException.class)
    public void testGet () throws IndexOutOfBoundsException {
        System.out.println("8-> devuelve la cadena localizada en la posición indicada");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);
        Object expResult1 = "uno";
        Object result1 = (String) instance.get(0);
        assertEquals(expResult1, result1);       
        
        instance.get(-4);
        String expResult2 = "Posicion no valida";
        IndexOutOfBoundsException result2 = new IndexOutOfBoundsException("Posicion no valida");
        assertEquals(expResult2, result2);
    }
      
    // añade la cadena al final del array. En caso de que no 
    // existiera sitio disponible, se incrementará el tamaño del array en un 50% 
    @Test
    public void testAddToEnd(){
        System.out.println("9-> Se añade cadena al final del array");
        Object[] s = {"uno","dos","cuatro"};
        DynArray instance = new DynArray(s);
        assertEquals(s.length,instance.getLength());
        instance.add("cero");
        assertEquals(s.length+1,instance.getLength());
        Object expResult = "cero";
        Object result = instance.get(instance.size()-1);
        assertEquals(expResult, result);        
        instance.add(5);
        Object expResult2 =5;    
        Object result2 =  instance.get(instance.size()-1);
        assertEquals(expResult2, result2);
        
    }
//    //inserta la cadena en la posición indicada. En caso 
//    //de que la posición indicada sea incorrecta, lanzará una excepción 
//    //IndexOutOfBoundsException​ con el mensaje ​“Posición no válido: ”​ y la 
//    //posición solicitada. En caso de que no existiera sitio disponible, se 
//    //incrementará el tamaño del array en un ​50%​.
    @Test (expected = java.lang.IndexOutOfBoundsException.class)
    public void testAddAnyPosition() throws IndexOutOfBoundsException​{
        System.out.println("10-> Se añade cadena al final del array");
        Object[] s = {"uno","dos","cinco"};
        DynArray instance = new DynArray(s);
        instance.add(0,"0");
        int expResult = 4;        
        int result = instance.size();
        assertEquals(expResult, result);        
        Object expResult4 = "0";
        Object result4 =  instance.get(0);
        assertEquals(expResult4,result4);
        
        instance.add(1,4);
        Object expResult2 = 4;  
        Object result2= instance.get(1);
        assertEquals(expResult2,result2);
        
        instance.add(-4,"-4");
        String expResult3 = "Posicion no valida";
        IndexOutOfBoundsException result3 = new IndexOutOfBoundsException("Posicion no valida");
        assertEquals(expResult3, result3);
    }
//        //modifica el valor en la posición indicada con la 
//    //nueva cadena. En caso de que la posición indicada sea incorrecta, 
//    //lanzará una excepción ​IndexOutOfBoudsException​ con el mensaje 
//    //“Posición no válido: ” ​y la posición solicitada. 
    @Test (expected = java.lang.IndexOutOfBoundsException.class)
    public void testSet() throws IndexOutOfBoundsException{
        System.out.println("11-> modifica el valor en la posición indicada con la nueva cadena");
        Object[] s = {"uno","dos","cinco"};
        DynArray instance = new DynArray(s);
        instance.set(0,"1");
        Object expResult = "1";        
        Object result = instance.get(0);
        assertEquals(expResult, result);
        
        instance.set(2,"2");
        assertEquals(3, instance.size());
        assertEquals(2,instance.indexOf("2"));
        
        instance.set(-1, "cinco");
        Object expResult2 = "Posicion no valida";
        IndexOutOfBoundsException result2 = new IndexOutOfBoundsException("Posicion no valida");
        assertEquals(expResult2, result2);
    }
//    
//    //devuelve la cadena de la posición indicada y la elimina 
//    //del array. En caso de que la posición indicada sea incorrecta, lanzará 
//    //una excepción ​IndexOutOfBoudsException​ con el mensaje ​“Posición no 
//    //válido: ”​ y la posición solicitada.  
    @Test  (expected = java.lang.IndexOutOfBoundsException.class)
    public void testRemove() throws IndexOutOfBoundsException{
        System.out.println("12-> devuelve la cadena de la posición indicada y la elimina del array");
        Object[] s = {"uno","dos","cinco"};
        DynArray instance = new DynArray(s);
        Object expResult = "uno";        
        Object result = instance.remove(0);
        assertEquals(expResult, result);          
        
        instance.remove(-1);
        Object expResult2 = "Posicion no valida";
        IndexOutOfBoundsException result2 = new IndexOutOfBoundsException("Posicion no valida");
        assertEquals(expResult2, result2);
    }
    
     //devuelve la ​posición​ en la que se encuentra la cadena 
     //indicada en el array, o ​-1​ en caso de no encontrarla 
    @Test
    public void testIndexOf(){
        System.out.println("13-> devuelve la​posición en la que se encuentra la cadena o -1 si no la encuentra");
        Object[] s = {"uno","dos","cinco"};
        DynArray instance = new DynArray(s);        
        int expResult = 1;        
        int result = instance.indexOf("dos");
        assertEquals(expResult, result);  
        
        int expResult2 = -1;        
        int result2 = instance.indexOf("hola");
        assertEquals(expResult2, result2);  
    }
   
    //elimina la cadena del array. Devuelve un valor 
    //indicando si la operación tuvo éxito o no.
    @Test  
    public void testDelete()  {
        System.out.println("14-> elimina la cadena del array. Devuelve un valor indicando si la operacion tuvo exito o no");
        Object[] s = {"uno","dos","cinco"};
        DynArray instance = new DynArray(s);              
        boolean result = instance.delete("dos");       
        assertTrue(result); 
        
        boolean result2 = instance.delete("hola");
        assertFalse(result2);       
    }
 
    @Test
    public void testToString(){
        System.out.println("15-> Imprimir elementos de array");
        Object[] s= {"uno","dos","tres"};
        DynArray instance = new DynArray(s);
        String expResult ="[ uno, dos, tres ]";
        String result = instance.toString();
        assertEquals(expResult,result);       
     
        instance.clear();
        Object expResult3 ="No hay elementos.";
        Object result3 = instance.toString();
        assertEquals(expResult3,result3);        
        
        DynArray instance2= new DynArray();
        Object expResult2 = "No hay elementos.";
        Object result2 = instance2.toString();
        assertEquals(expResult2,result2);        
    }
}
